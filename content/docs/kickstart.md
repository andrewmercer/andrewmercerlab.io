---
title: Kickstart Knowledge Dump
---
* https://github.com/rhinstaller/anaconda
* https://docs.centos.org/en-US/8-docs/advanced-install

# Create Kickstart files

## My Kickstart examples
* [My Kickstart examples](https://gitlab.com/andrewmercer/public/-/tree/main/kickstart)

## Using and modifying the automatically generated
Any manual install of rhel or centos will generate a kickstart from the options you chose during installation and store them in /root (anaconda-ks.cfg  ks_post.log  original-ks.cfg)


## Red Hat's Kickstart generator
* [Kickstart generator (for Red Hat subscribers only I think)](https://access.redhat.com/labs/kickstartconfig)

## View all available kickstart options in source code
```
git clone https://github.com/rhinstaller/anaconda && \
cd anaconda

view pyanaconda/core/kickstart/commands.py
```

## Complex partitioning 
* [Online resource for complex partitioning](https://dark.ca/2009/08/03/complex-partitioning-in-kickstart)

# Making Kickstart files available

## PXE boot server
This is the preferred method, will need to track down my notes on how to create one.

## Retrieve kickstart from a public GitLab repository

```
linux inst.ks=https://git.andrewmercer.net/amercer/kickstart/raw/master/server_ks.cfg [ networking_options ]
```

## Retrieve kickstart file from a private GitLab repository

### Retrieve project and file ID and private token

```
# Project ID
curl https://git.andrewmercer.net/api/v3/projects?private_token=[ my_private_token ] | grep -i kickstart

# File ID
curl https://git.andrewmercer.net/api/v3/projects/145/repository/tree?private_token=[ my_private_token ]
```

### Retrieve the kickstart

```
linux inst.ks=https://[ gitlab_user ]:[ gitlab_password ]@git.andrewmercer.net/api/v3/projects/145/repository/raw_blobs/<file_id>?private_token=[ my_private_token ] [ networking_options ]
```

## Create a custom ISO with a kickstart on it

```
KS_DIR="/build/RHEL7/isolinux/ks"
mkdir -p $KS_DIR
cp [ kickstart_file ]_ks.cfg $KS_DIR/ks.cfg
sudo mkisofs -o <hostname>.iso -b isolinux.bin -c boot.cat -no-emul-boot -V 'CentOS 7 x86_64' -boot-load-size 4 -boot-info-table -R -J -v -T /build/RHEL7/isolinux/
```

## Retrieve kickstart file from a mounted ISO (cdrom)

```
linux inst.ks=cdrom:/dev/cdrom:/ks/[ kickstart_file ].cfg [ networking_options ]
```

## Retrieve kickstart file from http

```
linux inst.ks=http://[ server_address ]/kickstart/[ kickstart_file ].cfg [ networking_options ]
```

### Server with a bonded interface restriction
```
linux inst.ks=http://[ server_address ]/ks/[ kickstart_file ].cfg [ networking_options ] bond=bond0:ens160,ens192:mode=802.3ad,primary=ens160
```

## Retrieve kickstart file from floppy
(VMware floppy image)

```
linux inst.ks=hd:fd0:/[ kickstart_file ].cfg [ networking_options ]
```

## Retrieve kickstart file from image file
(DRAC image file)

```
linux inst.ks=hd:/dev/sdb:/[ kickstart_file ].cfg [ networking_options ]
```

# Specify networking options for the server
* https://anaconda-installer.readthedocs.io/en/latest/boot-options.html#network-options

```
ip=192.168.1.238::192.168.1.1:255.255.255.0:[ my_hostname ]:ens160:none nameserver=192.168.1.1

ip=<ip>::<gateway>:<netmask>:<hostname>:<interface>:none nameserver=<nameserver_ip>
```

# Kickstart tools

```
sudo dnf install pykickstart
```

## ksvalidator
Lint your kickstart files

```
ksvalidator ~/kickstart/standard_rhel7_virt_ks.cfg
```

## ksverdiff
See changes between Red Hat versions

```
ksverdiff -f rhel7 -t rhel8
```

## ksflatten
I haven't used this tool, but more information can be found in the man page

```
man ksflatten
```

## ksshell
ksshell  is an interactive kickstart shell. I haven't used this tool, but more information can be found in the man page

```
man ksshell
```

# Troubleshooting Kickstart installations

## SSH into a kickstarting instance

### Get a list of current dhcp leases on your hypervisor

```
arp -an > leases_before.txt
```

### Add inst.sshd=1 to the --extra-args section of your virt-install command
For example:

```
sudo virt-install --name cluster0 --description "cluster0" --memory=1024 --vcpus=1 --location ~/CentOS-7-x86_64-Minimal-1908.iso \
--os-type=linux --os-variant=rhel7 --disk pool=default,bus=virtio,size=10 --network bridge=br0,model=virtio --graphics none --console=pty,target_type=serial \
--extra-args "console=ttyS0,115200n8 inst.sshd=1 ks=http://192.168.0.50:8000/standard_ks.cfg ip=dhcp"
```

### Once kickstart has passed point of acquiring a dhcp lease, get a new list of dhcp leases on hypervisor

```
arp -an > leases_after.txt
sdiff leases_before.txt leases_after.txt
```

### ssh into the kickstarting instance

```
ssh root@[ instance_ip ]
```

Shouldn't require a password.

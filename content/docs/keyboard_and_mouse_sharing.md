---
title: Keyboard & Mouse Sharing with Fedora Linux
---

# tl;dr

I recently did a fresh install of Fedora 40 on a desktop and struggled to get keyboard & mouse sharing working. Hopefully these sparse notes can help someone else out, but in the end I'm now using [Deskflow](https://github.com/deskflow/deskflow) (which is apparently now the upstream project for Synergy) to serve keyboard & mouse and an older version of [Barrier](https://github.com/debauchee/barrier) that just happens to still work as a client

Products mentioned in this document:

* [Synergy](https://symless.com/synergy)
* [Barrier](https://github.com/debauchee/barrier)
* [input-leap](https://github.com/input-leap/input-leap)
* [Deskflow](https://github.com/deskflow/deskflow)

# Path I took here

Synergy => Barrier => input-leap => Deskflow/Barrier

I've been using kvm software for years and for the longest time Synergy was the standout product. I don't recall exactly when, but Barrier replaced it and Barrier worked great. But I guess development had stalled so it was forked to input-leap at some point. This is pretty vague, but I remember upgrading Fedora somewhere within the last few releases and Barrier just didn't exist anymore (at least not in the repos). Some brief online searching brought me to input-leap and I'd honestly just started to remember what it's called when low and behold, when I installed Fedora 40, it just started crashing when clicking the Configure button.

There does appear to be a fix in place, but I guess it wasn't merged upstream. So I decided to build from source. I'm going to share my build-from-source experience, but **spoiler alert**, it didn't end up working after all was said and done.

# Build input-leap from source

## Download input-leap source

* https://github.com/input-leap/input-leap

```
git clone https://github.com/input-leap/input-leap.git
```

## Run the clean_build.sh script

```
./clean_build.sh
```

## Fix dependency errors

### First error: can't find Qt6Config

```
CMake Error at CMakeLists.txt:78 (find_package):
  By not providing "FindQt6.cmake" in CMAKE_MODULE_PATH this project has
  asked CMake to find a package configuration file provided by "Qt6", but
  CMake did not find one.

  Could not find a package configuration file provided by "Qt6" with any of
  the following names:

    Qt6Config.cmake
    qt6-config.cmake

  Add the installation prefix of "Qt6" to CMAKE_PREFIX_PATH or set "Qt6_DIR"
  to a directory containing one of the above files.  If "Qt6" provides a
  separate development package or SDK, be sure it has been installed.


-- Configuring incomplete, errors occurred!
```

I had to download the Qt development kit and signup

* https://www.qt.io/download-qt-installer-oss

```
chmod +x qt-online-installer-linux-x64-4.8.1.run
./qt-online-installer-linux-x64-4.8.1.run
```

- Selected Desktop Development option
- It installed to ~/Qt

```
export Qt6_DIR="/home/amercer/Qt/6.8.2/gcc_64"
```

This fixed the Qt6Config error

However ...

### Second error: can't find avahi-compat-libdns_sd

```
-- Checking for module 'avahi-compat-libdns_sd'
--   Package 'avahi-compat-libdns_sd', required by 'virtual:world', not found
CMake Error at /usr/share/cmake/Modules/FindPkgConfig.cmake:645 (message):
  The following required packages were not found:

   - avahi-compat-libdns_sd

Call Stack (most recent call first):
  /usr/share/cmake/Modules/FindPkgConfig.cmake:873 (_pkg_check_modules_internal)
  CMakeLists.txt:157 (pkg_check_modules)


-- Configuring incomplete, errors occurred!
```

If I install avahi-compat-libdns_sd, I get the same error, so where is it ... oh wait ...

```
sudo dnf install avahi-compat-libdns_sd avahi-compat-libdns_sd-devel
```

... needs the devel package *facepalm*, duh.

### Third error

```
CMake Error at /usr/share/cmake/Modules/FindPkgConfig.cmake:645 (message):
  The following required packages were not found:

   - x11
   - xext
   - xrandr
   - xinerama
   - xtst
   - xi

Call Stack (most recent call first):
  /usr/share/cmake/Modules/FindPkgConfig.cmake:873 (_pkg_check_modules_internal)
  CMakeLists.txt:163 (pkg_check_modules)


-- Configuring incomplete, errors occurred!
```

```
sudo dnf install libX11-devel libXext-devel libXrandr-devel libXinerama-devel libXtst-devel libXi-devel
```

### Fourth error

```
CMake Error at /usr/share/cmake/Modules/FindPkgConfig.cmake:645 (message):
  The following required packages were not found:

   - ice
   - sm

Call Stack (most recent call first):
  /usr/share/cmake/Modules/FindPkgConfig.cmake:873 (_pkg_check_modules_internal)
  CMakeLists.txt:164 (pkg_check_modules)


-- Configuring incomplete, errors occurred!
```

```
sudo dnf install libICE-devel libSM-devel
```

### Fifth error

```
Make Error at /usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:233 (message):
  Could NOT find OpenSSL, try to set the path to OpenSSL root folder in the
  system variable OPENSSL_ROOT_DIR (missing: OPENSSL_CRYPTO_LIBRARY
  OPENSSL_INCLUDE_DIR SSL Crypto) (Required is at least version "1.1.1")
Call Stack (most recent call first):
  /usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:603 (_FPHSA_FAILURE_MESSAGE)
  /usr/share/cmake/Modules/FindOpenSSL.cmake:689 (find_package_handle_standard_args)
  CMakeLists.txt:258 (find_package)


-- Configuring incomplete, errors occurred!
```

```
sudo dnf install openssl-devel
```

### Sixth and final error

```
CMake Warning at /usr/share/cmake/Modules/CMakeFindDependencyMacro.cmake:76 (find_package):
  Found package configuration file:

    /home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6Gui/Qt6GuiConfig.cmake

  but it set Qt6Gui_FOUND to FALSE so package "Qt6Gui" is considered to be
  NOT FOUND.  Reason given by package:

  Qt6Gui could not be found because dependency WrapOpenGL could not be found.

  Configuring with --debug-find-pkg=WrapOpenGL might reveal details why the
  package was not found.

  Configuring with -DQT_DEBUG_FIND_PACKAGE=ON will print the values of some
  of the path variables that find_package uses to try and find the package.

Call Stack (most recent call first):
  /home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6/QtPublicDependencyHelpers.cmake:145 (find_dependency)
  /home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6Widgets/Qt6WidgetsDependencies.cmake:45 (_qt_internal_find_qt_dependencies)
  /home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6Widgets/Qt6WidgetsConfig.cmake:43 (include)
  /home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6/Qt6Config.cmake:190 (find_package)
  src/gui/CMakeLists.txt:2 (find_package)


CMake Warning at /home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6/Qt6Config.cmake:190 (find_package):
  Found package configuration file:

    /home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6Widgets/Qt6WidgetsConfig.cmake

  but it set Qt6Widgets_FOUND to FALSE so package "Qt6Widgets" is considered
  to be NOT FOUND.  Reason given by package:

  Qt6Widgets could not be found because dependency Qt6Gui could not be found.

  Configuring with --debug-find-pkg=Qt6Gui might reveal details why the
  package was not found.

  Configuring with -DQT_DEBUG_FIND_PACKAGE=ON will print the values of some
  of the path variables that find_package uses to try and find the package.

Call Stack (most recent call first):
  src/gui/CMakeLists.txt:2 (find_package)


CMake Error at src/gui/CMakeLists.txt:2 (find_package):
  Found package configuration file:

    /home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6/Qt6Config.cmake

  but it set Qt6_FOUND to FALSE so package "Qt6" is considered to be NOT
  FOUND.  Reason given by package:
  Failed to find required Qt component "Widgets".

  Expected Config file at
  "/home/amercer/Qt/6.8.2/gcc_64/lib/cmake/Qt6Widgets/Qt6WidgetsConfig.cmake"
  exists



  Configuring with --debug-find-pkg=Qt6Widgets might reveal details why the
  package was not found.

  Configuring with -DQT_DEBUG_FIND_PACKAGE=ON will print the values of some
  of the path variables that find_package uses to try and find the package.



-- Configuring incomplete, errors occurred!
```

I did two things:

```
sudo dnf install mesa-libGL-devel
```

and opened the Qt SDK and added the following components

- Qt => Build tools => Qt Installer Framework
- Qt Creator => Plugin Development

After all this, the package finally built.

## Install input-leap

```
mkdir ~/.local/bin # (was a fresh install)
cp build/bin/input-leap* ~/.local/bin
input-leap
```

However, it would not work. Ports were open, I saw a client connect, matched sha fingerprints, but mouse would not switch between server & client no matter what I tried. So I stumbled across Deskflow and installed it

## Install Deskflow

* https://github.com/deskflow/deskflow

Worked very well out of the box except for one annoying issue:

* https://github.com/deskflow/deskflow/issues/8032

but this is manageable and there are eyes on it.

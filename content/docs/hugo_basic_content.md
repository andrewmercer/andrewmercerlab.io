---
title: Basic content creation in Hugo
---

* https://gohugo.io/getting-started/quick-start
* https://gohugo.io/getting-started/usage

# Content path

```
andrewmercer.gitlab.io/content
```

# Sample directory and content tree

```
tree -L 2 andrewmercer.gitlab.io/content
andrewmercer.gitlab.io/content
├── docs
│   ├── hugo_basic_content.md
│   ├── hugo_gitlab_pages_and_namecheap.md
│   ├── _index.md
│   ├── kickstart.md
│   └── technical_writing.md
└── technical
    ├── certifications.md
    ├── code_samples.md
    ├── _index.md
    └── resume.md
```

# Create a menu

```
# path: andrewmercer.gitlab.io/hugo.yml

baseURL: 'https://andrewmercer.gitlab.io'
relativeURLs: true
featured_image: 'images/Grand_River_2023-09_19.jpg'
languageCode: 'en-us'
omit_header_text: true
sectionPagesMenu: main
title: 'Andrew Mercer'
theme: 'ananke'

menu:
  main:
  - name: "Docs"
    identifier: "docs"
    name: "Docs"
    url: "/docs"
    weight: 20
  - name: "Technical"
    identifier: "technical"
    name: "Technical"
    url: "/technical"
    weight: 20
```

# Create _index files

```
# path: content/docs/_index.md

---
simple_list: true
summary: "Index page for docs"
title: "Docs"
type: docs
weight: 1
---
```

```
# path: content/technical/_index.md

---
simple_list: true
summary: "Index page for technical related things"
title: "Technical"
type: docs
weight: 1
---
```

# Generate content

```
hugo
```

# Start a local server to test changes

```
hugo server # starts server at 127.0.0.1:1313
hugo server --bind 0.0.0.0 # start server at 0.0.0.0:1313 (default port) for use with external access e.g. http://192.168.0.1:1313
hugo server --bind 0.0.0.0 -p 3013 # start server at 0.0.0.0:3013 (non-default port)
```

# Link to markdown (*.md) files in other markdown files

NOTE: Not sure how to document rel links without hugo treating them as actual links and failing.

<!-- <code>[My Resume]({{< ref "technical/resume.md" >}})</code> -->

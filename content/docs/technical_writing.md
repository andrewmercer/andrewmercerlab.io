---
title: Technical Writing Guide
---
It's been some time since I was serious about professional technical writing. I document practically everything on my own spare time as a personality trait, I have an immense collection of documents, but haven't adhered to any standards. This will be an on-going project to re-learn technical writing and keep track of resources, knowledge.

* https://developers.google.com/tech-writing/resources
* https://www.writethedocs.org/guide/docs-as-code

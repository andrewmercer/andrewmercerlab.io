---
title: Non-native invasive/noxious plants
---
# List of non-native invasive/noxious plants that I battle.

* [Rhamnus cathartica (Common buckthorn)](https://www.andrewmercer.net/ecology/common_buckthorn)
* [Vinca major (Greater periwinkle)](https://www.invadingspecies.com/invaders/plants/invasive-ground-covers-2)
* [Vinca minor (Lesser periwinkle)](https://www.invadingspecies.com/invaders/plants/invasive-ground-covers-2)
* [Digitaria ischaemum (Smooth crabgrass)](https://www.inaturalist.org/taxa/76658-Digitaria-ischaemum)
* [Setaria pumila (Yellow foxtail)](https://www.inaturalist.org/taxa/48688-Setaria-pumila)
* [Alliaria petiolata (Garlic Mustard)](https://www.invadingspecies.com/invaders/plants/garlic-mustard-2)
* [Heracleum maximum (Giant Hogweed)](https://www.invadingspecies.com/invaders/plants/giant-hogweed-2)
* [Arctium minus (Common Burdock)](https://www.inaturalist.org/taxa/59570-Arctium-minus)
* [Aegopodium podagraria (Goutweed)](https://www.ontarioinvasiveplants.ca/invasive-plants/species/goutweed)
* [Hedera helix (English Ivy)](https://www.invadingspecies.com/invaders/plants/invasive-ground-covers-2)
* [Taraxacum officinale (Common Dandelion)](https://www.inaturalist.org/taxa/47602-Taraxacum-officinale)
* [Plantago major (Greater Plantain)](https://www.inaturalist.org/taxa/58961-Plantago-major)
* [Lythrum salicaria (Purple Loosestrife)](https://www.ontarioinvasiveplants.ca/invasive-plants/species/purple-loosestrife)

# Not technically non-native invasive/noxious, but very aggressively relentless and frustrating

* [Cardamine hirsuta (Hairy Bittercress)](https://www.inaturalist.org/taxa/55829-Cardamine-hirsuta)
* [Erigeron canadensis (Canada Horseweed)](https://www.inaturalist.org/taxa/76907-Erigeron-canadensis)
* [Chelidonium majus (Greater Celandine)](https://www.inaturalist.org/taxa/55757-Chelidonium-majus) - this looks extremely similar to Wood Poppy
* [Ambrosia artemisiifolia (Common Ragweed)](https://www.inaturalist.org/taxa/53587-Ambrosia-artemisiifolia)
* [Thlaspi arvense (Field penny-cress)](https://www.inaturalist.org/taxa/79358-Thlaspi-arvense)
* [Barbarea vulgaris (Bitter wintercress/Yellow rocket)](https://www.inaturalist.org/taxa/56126-Barbarea-vulgaris)
* [Myosotis sylvatica (Wood Forget-me-not)](https://www.inaturalist.org/taxa/55826-Myosotis-sylvatic)
* [Complex Solanum nigrum (Black nighshade)](https://www.inaturalist.org/taxa/1351686-Solanum-nigrum)
* [Sonchus oleraceus (Common milk thistle)](https://www.inaturalist.org/taxa/53294-Sonchus-oleraceus)
* [Ajuga genevensis (Blue bugle)](https://www.inaturalist.org/taxa/61326-Ajuga-genevensis)

# Plants I find annoying, but may actually be beneficial

* [Acalypha virginica (Virginia Copperleaf/Three-seed Mercury)](https://www.inaturalist.org/taxa/119799-Acalypha-virginica)

# Resources
* [Invasive Ground Covers (English Ivy, Goutweed, Periwinkle)](https://www.invadingspecies.com/invaders/plants/invasive-ground-covers-2)
* [Ontario Spring Weeds](https://horticultureforhomegardeners.ca/2021/04/24/20-spring-weeds-in-ontario)
* [Give Weeds a Chance](https://www.humanegardener.com/give-weeds-animals-chance)

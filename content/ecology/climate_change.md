---
title: Climate Change
---

* [The Science of Climate Change Explained: Facts, Evidence and Proof](https://archive.ph/y8k06#selection-355.0-355.66)
* [Exxon Knew About Climate Change Almost 40 Years Ago](https://www.scientificamerican.com/article/exxon-knew-about-climate-change-almost-40-years-ago)
* [MIT Climate Primer](https://climateprimer.mit.edu)
* [Climate Change: The Science and Global Impact](https://learning.edx.org/course/course-v1:SDGAcademyX+CCSI001+3T2022/home)
* [Climate Prediction Citizen Science Climate Monitoring](https://www.climateprediction.net/getting-started/)

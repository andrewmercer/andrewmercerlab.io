---
title: Common Buckthorn
---
* [Rhamnus cathartica (Common buckthorn)](https://www.ontarioinvasiveplants.ca/invasive-plants/species/buckthorn)

# Management techniques

* https://www.ontarioinvasiveplants.ca/wp-content/uploads/2016/06/OIPC_BMP_Buckthorn.pdf

## Pulling

## Girdling

## Mowing

## Chemical treatment
- herbicide

# Proper disposal
- directly into landfill channels, not compost

# Prevention
* [Toronto's Turfgrass & Prohibited Plants bylaw](https://www.toronto.ca/city-government/public-notices-bylaws/bylaw-enforcement/turfgrass-prohibited-plants)

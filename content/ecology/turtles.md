---
title: Testudines (Turtles)
---
# Ontario turtle resources

## Turtle rescue and habitat protection

### Ontario Turtle Hotline

#### In case of nest or turtle in danger

* [RARE Sites Turtle Hotline](https://raresites.org/conservation/flora_fauna/protect-the-turtles)

#### In case of injured turtle 

* [Ontario Turtle Contact](https://ontarioturtle.ca/contact)
* [Ontario Turtle Drop-off](https://ontarioturtle.ca/ourmission/drop-off)
* [Scalese Nature Park Contact](https://www.scalesnaturepark.ca/contact-us)

### Turtle Nest Protectors

* [Ontario Turtle Nest Protector Information](https://ontarioturtle.ca/get-involved/turtle-nests-and-nest-protection)
* [Ontario Turtle Nest Protector Brochure](https://ontarioturtle.ca/wp-content/uploads/2021/06/OTCC-Brochure-Nest-Protectors.pdf)
* [How to install a Nest Protector](https://www.turtleskingston.com/nest-protection-program#installation)
* [Report turtle sightings or stewardship activity](https://ontarioturtle.ca/get-involved/turtle-nests-and-nest-protection)


## Turtle Conservation Organizations

* [Ontario Turtle](https://ontarioturtle.ca)
* [RARE](https://raresites.org/conservation/flora_fauna/protect-the-turtles)
* [Ontario Turtle Conservation Network](https://otcn.ca)
* [Canadian Wildlife Federation (CWF) Save Turtles At Risk (START) Program](https://cwf-fcf.org/en/explore/turtles/start-muskoka-turtle-project.html)
* [Canadian Wildlife Federation (CWF) Turtle Projects](https://cwf-fcf.org/en/explore/turtles)
* [Scales Nature Park](https://www.scalesnaturepark.ca)
* [Turtle Guardians](https://www.turtleguardians.com)
* [Turtles Kingston](https://www.turtleskingston.com)

## Ministry of Natural Resources & Forestry locations

* [MNRF Locations Map](https://mnrf.maps.arcgis.com/apps/Embed/index.html?webmap=94410085882f4a28a16f466e8cc94b51)

## Training

* [Scales Nature Park Herpetology Experience and Research Program](https://www.scalesnaturepark.ca/training)

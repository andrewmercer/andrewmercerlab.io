---
title: Resume
draft: true
---
* [My Resume]({{< ref "technical/resume.md" >}})

# ** Andrew Mercer **                   Email: **<andrew@andrewmercer.net>**
                                        Phone: **+1 226-750-3208**
                                        Website: **[andrewmercer.net](www.andrewmercer.net)**






# In progress

- In 2015/16, spearheaded and implemented a total automation of new server builds and developer tool install/config using Puppet. Began a cultural shift to DevOps practices (Puppet managed servers & applications). Extensive documentation of every piece of tech I managed. Encouraged automated, reproducible and repeatable error-free workflows based on resilience, well planned and well documented processes.

- In 2017/18, spearheaded cultural shift to git, ci/cd/cd, Ansible in a hybrid on-prem/AWS environment. Lead tech for all green/brown field projects, lead improvement and efficiency initiatives. Managed documentation of all projects. Mentored and empowered junior developers by improving their development workflow and toolset, encouraging hands-on operations, encouraged mistakes as learning opportunities (building resilience prior) and meticulous documentation and knowledge sharing.

- 2018-21, spearheaded team effort to create and use lab servers to reproduce client environment to ensure efficient troubleshooting playbooks

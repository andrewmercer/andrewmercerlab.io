---
title: Code Samples
---

* [Ansible code samples](https://gitlab.com/andrewmercer/public/-/tree/main/ansible)
* [Kickstart code samples](https://gitlab.com/andrewmercer/public/-/tree/main/kickstart)
* [Preseed code samples](https://gitlab.com/andrewmercer/public/-/tree/main/preseed)

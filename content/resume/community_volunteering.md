---
title: Community Involvement and Volunteering
---

* [Community Native Pollinator Garden](https://www.lovemyhood.ca/en/my-neighbourhood/my-hood.aspx) - was put on hold, re-initiating hopefully next year
* [Community Traffic Calming](https://www.kitchener.ca/en/roads-and-cycling/traffic-calming.aspx)
* [Volunteered for the 2021 Mike Morrice Campaign](https://mikemorrice.ca)
* [Native Pollinator Gardening](https://www.kitchener.ca/en/water-and-environment/pollinators.aspx)
* [Certified Monarch Waystation Garden](https://www.monarchwatch.org/waystations/)
* [Certified Wildlife Habitat Garden](https://cwf-fcf.org/en/explore/gardening-for-wildlife/action/get-certified/)
* [Protecting Nesting Snapping Turtle, Installing and Maintaining Nest Protect in 2023](https://raresites.org/conservation/flora_fauna/protect-the-turtles)
* [Yearly Neighborhood and Green Space Litter Cleanups](https://www.earthday.org/actions/follow-our-cleanup-checklist)
* [Seed Library Donating](https://www.rwlibrary.ca/en/programs-events/seed-library.aspx) - researching proper seed collection techniques for next year

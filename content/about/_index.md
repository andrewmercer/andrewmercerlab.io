---
title: About
---

This site is a way for me to share a bit about myself and to also do some knowledge sharing.

Linux system administrator and open source aficionado with a focus on streamlining and perfecting development operations. Passionate about creating highly available, scalable infrastructure powered by open source, the cloud, automation tools and a work-smart, collaborative approach to development. I enjoy learning new things, gathering and organizing information and sharing what I've learned. I am process oriented and document practically everything.

Interested in expanding my skillset into technical writing and project management domains.

Feel free to contact me on LinkedIn or you can send me an [email](andrew@andrewmercer.net) at my personal address.

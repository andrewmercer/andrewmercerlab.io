---
title: Bean Recipes
---

# Crispy black bean tacos

Inspired by [playswellwithbutter.com's crispy black bean taco recipe](https://playswellwithbutter.com/crispy-black-bean-tacos)

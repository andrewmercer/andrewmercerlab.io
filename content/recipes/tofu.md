---
title: Tofu Recipes
---

# Ingredients to have on hand to make tofu taste good

- thick soya sauce
- mirrin
- agave
- smoke
- 

# Utility Crispy Tofu

To use in a variety of different dishes or just as an appetizer.

Gotta track down the recipe

# Tofu Breakfast Sandwiches

Inspired by [Derek Sarno's breakfast sandwich recipe](https://www.youtube.com/watch?v=Re_E2lCzk8M)
